package ru.tsc.tambovtsev.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.tambovtsev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.tambovtsev.tm.api.endpoint.ITaskEndpointClient;
import ru.tsc.tambovtsev.tm.dto.request.*;
import ru.tsc.tambovtsev.tm.dto.response.*;

@NoArgsConstructor
public final class TaskEndpointClient extends AbstractEndpointClient implements ITaskEndpointClient {

    public TaskEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskClearResponse clearTask(@NotNull TaskClearRequest request) {
        return call(request, TaskClearResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCreateResponse createTask(@NotNull TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskListResponse listTask(@NotNull TaskListRequest request) {
        return call(request, TaskListResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowByIdResponse showTask(@NotNull TaskShowByIdRequest request) {
        return call(request, TaskShowByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("admin", "admin")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser().getEmail());

        final TaskEndpointClient taskClient = new TaskEndpointClient(authEndpointClient);

        System.out.println(taskClient.createTask(new TaskCreateRequest("rewq143", "tryrt123")));
        System.out.println(taskClient.listTask(new TaskListRequest()).getTasks());

        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));

        authEndpointClient.disconnect();
    }

}
