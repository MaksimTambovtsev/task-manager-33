package ru.tsc.tambovtsev.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.dto.request.*;
import ru.tsc.tambovtsev.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface ITaskEndpoint extends IEndpoint{

    @NotNull
    String NAME = "TaskEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ) {
        @NotNull final String wsdl =
                "http://" + host + ":" + port + "/" + NAME + "?WSDL";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(SPACE, PART);
        return Service.create(url, qName).getPort(ITaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskChangeStatusByIdRequest request
    );

    @NotNull
    @WebMethod
    TaskClearResponse clearTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskClearRequest request
    );

    @NotNull
    @WebMethod
    TaskCreateResponse createTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskCreateRequest request
    );

    @NotNull
    @WebMethod
    TaskListResponse listTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskListRequest request
    );

    @NotNull
    @WebMethod
    TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskRemoveByIdRequest request
    );

    @NotNull
    @WebMethod
    TaskShowByIdResponse showTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskShowByIdRequest request
    );

    @NotNull
    @WebMethod
    TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskUpdateByIdRequest request
    );

}
