package ru.tsc.tambovtsev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.dto.request.UserLoginRequest;
import ru.tsc.tambovtsev.tm.dto.request.UserLogoutRequest;
import ru.tsc.tambovtsev.tm.dto.request.UserProfileRequest;
import ru.tsc.tambovtsev.tm.dto.response.UserLoginResponse;
import ru.tsc.tambovtsev.tm.dto.response.UserLogoutResponse;
import ru.tsc.tambovtsev.tm.dto.response.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAuthEndpoint {

    @NotNull
    @WebMethod
    UserLoginResponse login(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLoginRequest request
    );

    @NotNull
    @WebMethod
    UserLogoutResponse logout(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLogoutRequest request
    );

    @NotNull
    @WebMethod
    UserProfileResponse profile(
            @WebParam(name = "request", partName = "request")
            @NotNull UserProfileRequest request
    );

}
