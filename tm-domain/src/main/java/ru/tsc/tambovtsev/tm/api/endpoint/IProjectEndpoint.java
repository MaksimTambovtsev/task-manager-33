package ru.tsc.tambovtsev.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.dto.request.*;
import ru.tsc.tambovtsev.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IProjectEndpoint extends IEndpoint {

    @NotNull
    String NAME = "ProjectEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ) {
        @NotNull final String wsdl =
                "http://" + host + ":" + port + "/" + NAME + "?WSDL";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(SPACE, PART);
        return Service.create(url, qName).getPort(IProjectEndpoint.class);
    }

    @NotNull
    @WebMethod
    ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectChangeStatusByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectCascadeRemoveResponse cascadeRemoveProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectCascadeRemoveRequest request
    );

    @NotNull
    @WebMethod
    ProjectClearResponse clearProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectClearRequest request
    );

    @NotNull
    @WebMethod
    ProjectCreateResponse createProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectCreateRequest request
    );

    @NotNull
    @WebMethod
    ProjectListResponse listProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectListRequest request
    );

    @NotNull
    @WebMethod
    ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectRemoveByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectShowByIdResponse showProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectShowByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectUpdateByIdRequest request
    );

}
